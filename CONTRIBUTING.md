The way to contribute to this project follows the usual path:

- make a PR with a good description what kind of issue it is solving

- wait for the review and if some changes are requested, try to make them
  within the next few weeks, otherwise the PR may be closed due to inactivity.