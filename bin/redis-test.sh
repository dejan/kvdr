#!/usr/bin/env bash
set -e

test_token=BABADEDAuikxWx0oPZYfPE3IXJ9BVlSC

# Populate redis
redis-cli -h localhost -n ${redis_db} -a ${test_token} < var/test-data.redis

number_of_keys=$(echo "keys *" | redis-cli -h localhost -n ${redis_db} -a ${test_token} | wc -l)
echo $number_of_keys

if [ $number_of_keys -ne 14 ]; then
    echo "Expected 14 keys, got ${number_of_keys}!"
    exit 1
fi

# Dump the data to a file
PYTHONPATH=. python3 -m kvdr.kvdr dump --file=redis-test.dump "redis://:${test_token}@localhost/${redis_db}"

# Delete data in the test database
echo "flushdb" | redis-cli -h localhost -n ${redis_db} -a ${test_token}

# Sleep for 5 seconds so one of the keys expires
sleep 5

PYTHONPATH=. python3 -m kvdr.kvdr load --file=redis-test.dump "redis://:${test_token}@localhost/${redis_db}"

number_of_keys=$(echo "keys *" | redis-cli -h localhost -n ${redis_db} -a ${test_token} | wc -l)
echo $number_of_keys

if [ $number_of_keys -ne 13 ]; then
    echo "Expected 13 keys, got ${number_of_keys}!"
    exit 2
fi
