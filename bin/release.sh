#!/usr/bin/env bash
set -e

# the the final flake8 and pytest
flake8
pytest

chlog_file_name=$(mktemp)
version_str=${major}.${minor}.${patch}
branch_name=release/${version_str}

git checkout -b ${branch_name}
echo "# ${major}.${minor}.${patch}" >> ${chlog_file_name}

# Generate new kvdr/version.py
envsubst < var/version.tpl > kvdr/version.py

# Generate new _metadata.json
envsubst < var/metadata.tpl > _metadata.json

# Generate new CHANGELOG
echo " " >> ${chlog_file_name}
echo "TODO" >> ${chlog_file_name}
echo " " >> ${chlog_file_name}
cat CHANGELOG >> ${chlog_file_name}
cp ${chlog_file_name} CHANGELOG
rm -f ${chlog_file_name}

git commit kvdr/version.py _metadata.json CHANGELOG -m "Release ${version_str}"
# TODO: Look at https://stackoverflow.com/a/55415521/876497 how to automate the GitLab release
git tag -a ${version_str} -m "Release ${version_str}"
git push origin ${version_str}

./setup.py bdist_wheel
