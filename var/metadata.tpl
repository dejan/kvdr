{
    "description": "KVDR - Key/Value Dump and Restore",
    "author": "Dejan Lekic",
    "url": "https://gitlab.com/dejan/kvdr",
    "download_url": "https://gitlab.com/dejan/kvdr",
    "author_email": "dejan.lekic@gmail.com",
    "version": "${major}.${minor}.${patch}",
    "packages": ["kvdr"],
    "scripts": [],
    "name": "kvdr",
    "entry_points": {"console_scripts": ["kvdr=kvdr.kvdr:main"]},
    "python_requires": ">=3.0",
    "long_description_content_type": "text/markdown",
    "classifiers": [
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Development Status :: 5 - Production/Stable",
        "Environment :: Console",
        "Intended Audience :: System Administrators",
        "Topic :: System :: Recovery Tools"
    ],
    "license": "BSD 3-Clause License",
    "project_urls": {
        "Documentation": "https://gitlab.com/dejan/kvdr",
        "Code": "https://gitlab.com/dejan/kvdr",
        "Issue tracker": "https://gitlab.com/dejan/kvdr/issues"
    }
}
