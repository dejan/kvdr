#!/usr/bin/env python3
import json
import os

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

# from setuptools import find_packages

try:  # for pip>=10
    from pip._internal.req import parse_requirements
except ImportError:  # for pip <=9.0.3
    from pip.req import parse_requirements


base_path = os.path.dirname(__file__)


def get_metadata():
    meta = json.load(open('_metadata.json', 'r'))
    return meta


def get_requirements(*path):
    req_path = os.path.join(*path)
    reqs = parse_requirements(req_path, session=False)
    return [str(ir.req) for ir in reqs]


def read_text(file_name: str):
    return open(os.path.join(base_path, file_name)).read()


# We keep here only those named arguments that can't be in the metadata.json
setup_config = {
    "install_requires": get_requirements(base_path, 'requirements.txt'),
    "long_description": read_text("README.md")
}

# For help with classifiers, see https://pypi.org/classifiers/
setup(**setup_config, **get_metadata())
