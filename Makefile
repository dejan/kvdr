# KVDR Makefile
#
# make wheel
#    Build a Python wheel. If successful you will find the file in the dist directory.
# make clean
#    Clean up everything
# make redis-test redis_db=14
#    Runs an integration test. Uses Redis database 14 for it. WARNING: it will delete all the data in
#    this particular database!

.PHONY: clean release redis-test

clean:
	rm -Rf dist
	rm -Rf build

# make release major=0 minor=9 patch=7
release:
	@./bin/release.sh

# make redis-test redis_db=14
redis-test:
	@./bin/redis-test.sh

# make wheel
wheel:
	@./setup.py bdist_wheel

# make pub2test major=0 minor=9 patch=7
pub2test:
	twine upload --repository-url https://test.pypi.org/legacy/ dist/kvdr-${major}.${minor}.${patch}-py3-none-any.whl

# make publish major=0 minor=9 patch=7
publish:
	twine upload dist/kvdr-${major}.${minor}.${patch}-py3-none-any.whl
