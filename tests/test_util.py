import re
import kvdr.util as _util
from io import StringIO


def test_version():
    assert re.match(".*\\.*.\\.*.", _util.version())


def test_get_redis_client():
    obj = _util.get_redis_client("test")
    assert len(obj.keys()) == 0
    obj2 = _util.get_redis_client("redis://localhost/8")
    assert obj2


def test_out():
    fobj = StringIO()
    _util.out("hello", " ", "world!", out_file=fobj)
    # "hello world!" + LF == 13 bytes
    assert len(fobj.getvalue()) == 13
