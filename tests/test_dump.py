from io import StringIO

import pytest

import kvdr.dump as _dump


@pytest.fixture(scope="module")
def fake_redis():
    from kvdr.util import get_redis_client
    fredis = get_redis_client("test")

    # we do here exactly what is in the var/test-data.redis file

    # string
    fredis.set("str_key1", "hello", ex=1234)
    fredis.set("str_key:subkey1", "foo")
    fredis.set("str_key:subkey2", "bar", ex=12345)
    fredis.set("str_expired_5", "expired", ex=5)
    fredis.set("str_expired_15", "expired", ex=15)
    fredis.set("str_expired_60", "expired", ex=60)

    # hash
    fredis.hset("hash_key", "key1", "val1")
    fredis.hset("hash_key", "key2", "val2")

    # set
    fredis.sadd("set_key", "val1", "val2")

    # sorted set
    fredis.zadd("noexp_sorted_set", {"banana": 3, "apple": 1, "cherry": 2})
    fredis.zadd("exp_sorted_set", {"banana": 3, "apple": 1, "cherry": 2})
    fredis.expire("exp_sorted_set", 8765)

    # list
    fredis.rpush("list_key", "one", "two", "three")
    fredis.rpush("bands", "UDO", "Judas Priest", "Megadeth")
    fredis.expire("bands", 54321)

    # can't think of any unsupported type, so I will just use string
    fredis.set("unknown_key", "42", ex=2345)

    return fredis


def test_out_record():
    fobj = StringIO()
    _dump._out_record_header(10, rfile=fobj)
    assert fobj.getvalue().startswith("RR:")


def test_out_key():
    fobj = StringIO()
    _dump._out_key_name(b"key_name", rfile=fobj, base64_encoded=True)
    assert fobj.getvalue().startswith("KK:")
    _dump._out_key_name("key_name", rfile=fobj, base64_encoded=False)
    assert fobj.getvalue().startswith("KK:")


def test_out_ttl(fake_redis):
    fobj = StringIO()
    _dump._out_ttl("str_key1", fake_redis, rfile=fobj)
    assert fobj.getvalue().startswith("MT:")


def test_out_record_type():
    fobj = StringIO()
    _dump._out_record_type("string", rfile=fobj)
    assert fobj.getvalue().startswith("TT:")


def test_out_string(fake_redis):
    fobj1 = StringIO()
    _dump._out_string(b"str_key1", fake_redis, base64_encoded=True, rfile=fobj1)
    assert fobj1.getvalue().startswith("VV:")
    fobj2 = StringIO()
    _dump._out_string(b"str_key1", fake_redis, base64_encoded=False, rfile=fobj2)
    assert fobj2.getvalue().startswith("VV:")


def test_out_hash(fake_redis):
    fobj1 = StringIO()
    _dump._out_hash(b"hash_key", fake_redis, base64_encoded=True, rfile=fobj1)
    assert fobj1.getvalue().startswith("HK:")
    fobj2 = StringIO()
    _dump._out_hash(b"hash_key", fake_redis, base64_encoded=False, rfile=fobj2)
    assert fobj2.getvalue().startswith("HK:")


def test_out_set(fake_redis):
    fobj = StringIO()
    _dump._out_set(b"set_key", fake_redis, base64_encoded=True, rfile=fobj)
    assert fobj.getvalue().startswith("SV:")


def test_out_sorted_set(fake_redis):
    fobj = StringIO()
    _dump._out_sorted_set(b"noexp_sorted_set", fake_redis, base64_encoded=True, rfile=fobj)
    assert fobj.getvalue().startswith("ZK:")
    _dump._out_sorted_set(b"exp_sorted_set", fake_redis, base64_encoded=True, rfile=fobj)
    assert fobj.getvalue().startswith("ZK:")


def test_out_list(fake_redis):
    fobj = StringIO()
    _dump._out_list(b"list_key", fake_redis, base64_encoded=True, rfile=fobj)
    assert fobj.getvalue().startswith("LV:")
    fobj2 = StringIO()
    _dump._out_list(b"list_key", fake_redis, base64_encoded=False, rfile=fobj2)
    assert fobj2.getvalue().startswith("LV:one")


def test_out_unknown(fake_redis):
    fobj1 = StringIO()
    _dump._out_unknown(b"unknown_key", fake_redis, base64_encoded=True, rfile=fobj1)
    assert fobj1.getvalue().startswith("UK:")
    fobj2 = StringIO()
    _dump._out_unknown(b"unknown_key", fake_redis, base64_encoded=False, rfile=fobj2)
    assert fobj2.getvalue().startswith("UK:")


def test_dump(fake_redis):
    no_file_no_b64 = _dump.dump(fake_redis, base64_encoded=False)
    assert no_file_no_b64
    no_file_b64 = _dump.dump(fake_redis)
    assert no_file_b64

    file_no_b64 = _dump.dump(fake_redis, file_name="/tmp/redis.dump", base64_encoded=False)
    assert file_no_b64
    file_b64 = _dump.dump(fake_redis, file_name="/tmp/redisb64.dump")
    assert file_b64
