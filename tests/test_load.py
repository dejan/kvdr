from io import StringIO

import pytest

import kvdr.load as _load


@pytest.fixture(scope="module")
def fake_redis():
    from kvdr.util import get_redis_client
    return get_redis_client("test")


@pytest.fixture()
def fake_redis_dump():
    redis_dump = """RR:------------------- 1
    KK:dWRv
    MT:1574960131.021017
    EX:91079
    TT:hash
    HK:Zm5hbWU=
    HK:VWRv
    HK:c25hbWU=
    HK:RGlya3NjaG5laWRlcg==
    HK:ZG9i
    HK:MTk1MjA0MDY=
    RR:------------------- 2
    KK:bm9leHBfdW5pcXVlX25hbWVz
    MT:1574960131.021267
    EX:-1
    TT:set
    SV:RHJhZ2Fu
    SV:RGVqYW4=
    SV:TWlsYW4=
    RR:------------------- 3
    KK:a2V5MQ==
    MT:1574960131.021421
    EX:-1
    TT:string
    VV:aGVsbG8=
    RR:------------------- 4
    KK:a2V5OnN1YmtleTI=
    MT:1574960131.021567
    EX:4659
    TT:string
    VV:YmFy
    RR:------------------- 5
    KK:YmFuZHM=
    MT:1574960131.021717
    EX:46635
    TT:list
    RR:------------------- 6
    KK:ZXhwX3VuaXF1ZV9jb2xvdXJz
    MT:1574960131.021884
    EX:-1
    TT:set
    SV:cmVk
    SV:Ymx1ZQ==
    SV:Z3JlZW4=
    SV:YmxhY2s=
    SV:d2hpdGU=
    RR:------------------- 7
    KK:a2V5OnN1YmtleTE=
    MT:1574960131.022047
    EX:-1
    TT:string
    VV:Zm9v
    """
    fake_file = StringIO()
    fake_file.write(redis_dump)
    fake_file.flush()
    fake_file.seek(0)  # make it ready for tests to read from
    return fake_file


def test_load_dict_off(fake_redis_dump, fake_redis):
    # not building the dictionary
    result = _load.load(fake_redis_dump, fake_redis)
    assert result["loaded"] > 0
    assert "data" not in result


def test_load_dict_on(fake_redis_dump):
    # dictionary building turned on
    fake_redis_dump.seek(0)  # move to the top of the "file"
    result = _load.load(fake_redis_dump, None, True)
    assert result["loaded"] > 0
    assert "data" in result
    data = result["data"]
    assert "key1" in data
    assert data["key1"] == "hello"
